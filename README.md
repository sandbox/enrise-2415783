CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------
Zabbix Monitoring is a simple module that can be used to monitor Drupal
updates. This helps you make sure that your Drupal sites aren't running on
outdated versions of either the core or the modules.

4 URIs are available for monitoring:

 * /zabbix-monitoring/updates/core/warning
 * /zabbix-monitoring/updates/core/critical
 * /zabbix-monitoring/updates/modules/warning
 * /zabbix-monitoring/updates/modules/critical


 * For a full description of the module, visit the project page:
   https://drupal.org/project/zabbix_monitoring

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/zabbix_monitoring


REQUIREMENTS
------------
This module does not require any other modules.


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * You should make sure a cron is running so updates are picked up.


CONFIGURATION
-------------
 * Configure user permissions in Administration » System » Zabbix Monitoring

 * Add the newly exposed URIs as webchecks to your Zabbix installation.


TROUBLESHOOTING
---------------
 * If updates aren't shown in Zabbix, makes sure you can view the URIs in your browser correctly.


MAINTAINERS
-----------
Current maintainers:
* Stefan van Essen (eXistenZ)


This project has been sponsored by:
 * Enrise
   A web development company specialized in building APIs and web services.
   Visit https://www.enrise.com for more information.
